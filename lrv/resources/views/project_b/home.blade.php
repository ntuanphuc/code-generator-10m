@extends('layouts.app7')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{--{{ __('You are logged in!') }}--}}
                    <ul>
                        <li class="list-unstyled"><h3>{{ 'Total Codes: ' . number_format($total_codes) }} - {{ $secret_code_extend }}</h3></li>
                        <li class="list-unstyled"> &nbsp;</li>
                        <li class="list-unstyled"><a href="{{ route('b.start') }}">+ Add more codes</a></li>
                        <li class="list-unstyled"> &nbsp;</li>
                        <li class="list-unstyled"><a href="{{ route('b.export_data') }}">EXPORT Codes</a></li>
                        <li class="list-unstyled"> &nbsp;</li>
                        <li class="list-unstyled"><a href="{{ route('b.check_valid_code') }}">Check code valid</a></li>
                        <li class="list-unstyled"> &nbsp;</li>
                        <li class="list-unstyled"><a href="{{ route('b.config') }}">Config Extend Code</a></li>
                        <li class="list-unstyled"> &nbsp;</li>
                        <li class="list-unstyled"> &nbsp;</li>
                        <li class="list-unstyled"><a href="{{ route('b.reset_data') }}" class="text-danger">Wanna reset all codes?</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
