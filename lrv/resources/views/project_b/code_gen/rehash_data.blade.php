@extends('layouts.app7')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Reset Codes') }}</div>

                    <div class="card-body">
                        @if($processing)
                            <div class="alert alert-danger" role="alert">
                                There are {{ number_format($processing) }} codes are being processed

                            </div>
                            <p>
                                <a href="{{ route('b.home') }}" class="text-primary">Go back</a> or
                                <a href="{{ route('b.rehash_data', ['parcel' => $parcel]) }}" class="text-primary">Reload</a>
                            </p>
                        @else
                            @if ($deleted)
                                <div class="alert alert-success" role="alert">
                                    All data deleted
                                </div>
                            @endif
                            <form method="POST">
                                @csrf
                                <div class="form-group row">
                                    <label
                                        class="col-md-4 col-form-label text-md-right"></label>
                                    <div class="col-md-6">
                                        <span id="total_span">Enter "<span class="text-success">Yes, do rehash</span>" to text box to rehash your data</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="desire"
                                           class="col-md-4 col-form-label text-md-right">Verification string</label>

                                    <div class="col-md-6">
                                        <input id="secure_code" type="text"
                                               class="form-control @error('secure_code') is-invalid @enderror"
                                               name="secure_code"
                                               value="{{ old('secure_code') }}" required autofocus>

                                        @error('secure_code')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="desire"
                                           class="col-md-4 col-form-label text-md-right">New Secure Code (<span
                                            class="text-primary">{{ $config->key_value }}</span> )</label>

                                    <div class="col-md-6">
                                        <input id="secret_code" type="text"
                                               class="form-control @error('secret_code_extend') is-invalid @enderror"
                                               name="secret_code_extend"
                                               value="{{ old('secret_code_extend') }}" required autofocus>

                                        @error('secret_code_extend')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror

                                    </div>
                                </div>


                                <div class="form-group row mb-0" id="submit-btn-area">
                                    <div class="col-md-8 offset-md-4">
                                        <button class="btn btn-danger" type="submit">
                                            {{ __('Rehash Codes') }}
                                        </button>

                                        <a class="btn btn-link" href="{{ route('b.home') }}">
                                            {{ __('Cancel') }}
                                        </a>

                                    </div>
                                </div>
                            </form>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
