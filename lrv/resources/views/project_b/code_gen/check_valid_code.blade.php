@extends('layouts.app7')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Check if code is valid') }}</div>

                    <div class="card-body">
                        @if ($msg)
                            <div class="alert alert-success" role="alert">
                                {{ $msg }}
                            </div>
                        @endif
                        <form method="POST">
                            @csrf

                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right">Secret Hash</label>

                                <div class="col-md-6">

                                    <input id="parcel" type="text"
                                           class="form-control @error('secret_code_extend') is-invalid @enderror" name="secret_code_extend"
                                           value="{{ $secret_code_extend }}" required>

                                    @error('secret_code_extend')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right">Code</label>

                                <div class="col-md-6">

                                    <input id="parcel" type="text"
                                           class="form-control @error('code') is-invalid @enderror" name="code"
                                           value="{{ $code }}" required autofocus>

                                    @error('code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right">Hash</label>

                                <div class="col-md-6">

                                    <input id="parcel" type="text"
                                           class="form-control @error('hash') is-invalid @enderror" name="hash"
                                           value="{{ $hash }}" required autofocus>

                                    @error('hash')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0" id="submit-btn-area">
                                <div class="col-md-8 offset-md-4">
                                    <button class="btn btn-danger" type="submit">
                                        {{ __('Check') }}
                                    </button>

                                    <a class="btn btn-link" href="/b/home">
                                        {{ __('Cancel') }}
                                    </a>

                                </div>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
