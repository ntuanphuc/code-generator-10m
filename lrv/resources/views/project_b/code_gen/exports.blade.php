@extends('layouts.app7')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Export Codes') }}</div>

                    <div class="card-body d-none">
                        @if (0)
                            <div class="alert alert-success" role="alert">
                                All data deleted
                            </div>
                        @endif
                        <form method="POST">
                            @csrf

                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right">Parcel</label>

                                <div class="col-md-6">
                                    <input id="parcel" type="text"
                                           class="form-control @error('parcel') is-invalid @enderror" name="parcel"
                                           value="{{ old('parcel') }}" required autofocus>

                                    @error('parcel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right">No of Codes</label>

                                <div class="col-md-6">
                                    <input id="no_of_codes" type="text"
                                           placeholder="maximum 500.000 per file"
                                           class="form-control @error('no_of_codes') is-invalid @enderror"
                                           name="no_of_codes"
                                           value="{{ old('no_of_codes') }}" required autofocus>

                                    @error('no_of_codes')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0" id="submit-btn-area">
                                <div class="col-md-8 offset-md-4">
                                    <button class="btn btn-danger" type="submit">
                                        {{ __('Export') }}
                                    </button>

                                    <a class="btn btn-link" href="/home">
                                        {{ __('Cancel') }}
                                    </a>

                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="card-body">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-1 col-form-label text-md-right"></label>

                                <div class="col-md-9">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th class="">Parcel (so_lo)</th>
                                            <th class="">Total</th>
                                            <th class="">Export Origin</th>
                                            <th class="">Export Hash</th>
                                            <th class="">Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @if($parcels)
                                            @foreach($parcels as $pc)
                                                <tr id="tr_{{ trim($pc->so_lo) }}">
                                                    <td>{{ $pc->so_lo }}</td>
                                                    <td>{{ number_format($pc->every_total) }}</td>
                                                    <td>
                                                        <form method="POST" class="d-none">
                                                            @csrf
                                                            <input type="hidden" name="export_type" value="origin">
                                                            <input type="hidden" name="parcel" value="{{ $pc->so_lo }}">
                                                            <input type="hidden" name="no_of_codes" value="500000">
                                                            <input type="submit" value="Export" class="btn btn-link">
                                                        </form>
                                                        <a href="javascript:void(0)"
                                                           onclick="goExport('{{ trim($pc->so_lo) }}', 'origin')">Export</a>
                                                        <div class="spinner-border text-success d-none" role="status"
                                                             id="spinner-{{ trim($pc->so_lo) }}-origin">
                                                        </div>

                                                        @if(is_file(base_path('public/7.' . $pc->so_lo . '-origin.csv')))
                                                            | <a target="_blank"
                                                                 href="/7.{{  $pc->so_lo . '-origin.csv' }}">Download</a>
                                                        @endif

                                                    </td>
                                                    <td>
                                                        <form method="POST" class="d-none">
                                                            @csrf
                                                            <input type="hidden" name="export_type" value="hash_only">
                                                            <input type="hidden" name="parcel" value="{{ $pc->so_lo }}">
                                                            <input type="hidden" name="no_of_codes" value="500000">
                                                            <input type="submit" value="Export" class="btn btn-link">
                                                        </form>
                                                        <a href="javascript:void(0)"
                                                           onclick="goExport('{{ trim($pc->so_lo) }}', 'hash-only')">Export</a>
                                                        <div class="spinner-border text-success d-none" role="status"
                                                             id="spinner-{{ trim($pc->so_lo) }}-hash-only">
                                                        </div>
                                                        @if(is_file(base_path('public/7.' . $pc->so_lo . '-hash-only.csv')))
                                                            | <a target="_blank"
                                                                 href="/7.{{  $pc->so_lo . '-hash-only.csv' }}">Download</a>
                                                        @endif
                                                        | <a href="{{ route('b.rehash_data', ['parcel' => trim($pc->so_lo)]) }}">Re-hash</a>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(0)"
                                                           onclick="goDelete('{{ trim($pc->so_lo) }}')"
                                                           class="text-danger">Delete</a>
                                                        <div class="spinner-border text-success d-none" role="status"
                                                             id="spinner-{{ trim($pc->so_lo) }}-delete">
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('js-footer')
    <script type="text/javascript">
        function goExport(parcel, export_type) {
            let spinner_id = '#spinner-' + parcel + '-' + export_type;
            $(spinner_id).removeClass('d-none');
            axios.post('/b/exports', {
                parcel: parcel,
                export_type: export_type
            }).then((response) => {
                $(spinner_id).addClass('d-none');
                window.location.reload();
            }, (error) => {
                $(spinner_id).addClass('d-none');
            });
            return false;
        }

        function goDelete(parcel) {
            let r = confirm("Are you sure to delete " + parcel);
            if (r == true) {
                let spinner_id = '#spinner-' + parcel + '-delete';
                axios.post('/b/delete', {
                    parcel: parcel
                }).then((response) => {
                    if (typeof response.data.error == 'undefined') {
                        $(spinner_id).addClass('d-none');
                        $('#tr_' + parcel).hide();
                    } else {
                        alert(response.data.error.message);
                    }
                }, (error) => {
                    $(spinner_id).addClass('d-none');
                });
                return false;
            } else {

            }
        }
    </script>
@endsection
