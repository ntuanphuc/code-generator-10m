<table>
    <thead>
    <tr>
        <th>#</th>
        <th>Parcel</th>
        <th>Series</th>
        @if($export_type == 'origin')
        <th>Code</th>
        @endif
        @if($export_type == 'hash_only')
        <th>Hash</th>
        @endif
    </tr>
    </thead>
    <tbody>
    @if($codes)
        @php $i = 0; @endphp
        @foreach($codes as $c)
            @php
            $i++;
            $len = strlen($c->id);
            $addition = '';
            for($i1 = 0; $i1 < 8 - $len; $i1++){
                $addition .= '0';
            }
            $series = $c->series_prefix . $addition . $c->id;
            @endphp
            <tr>
                <td>{{ $i }}</td>
                <td>{{ $c->so_lo }}</td>
                <td>{{ $series }}</td>
                @if($export_type == 'origin')
                <td>{{ $c->code }}</td>
                @endif
                @if($export_type == 'hash_only')
                <td>{{ $c->hash_code }}</td>
                @endif
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
