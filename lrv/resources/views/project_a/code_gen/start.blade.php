@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Code Generator') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST">
                            @csrf
                            <div class="form-group row">
                                <label
                                    class="col-md-4 col-form-label text-md-right">Total codes</label>
                                <div class="col-md-6">
                                    <span id="total_span">{{ number_format($total_codes) }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right">{{ __('No of Code?') }}</label>

                                <div class="col-md-6">
                                    <input id="desire" type="email"
                                           class="form-control @error('desire') is-invalid @enderror" name="desire"
                                           value="{{ old('desire') }}" required autofocus>

                                    @error('desire')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">

                                <label for="parcel"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Parcel') }}</label>

                                <div class="col-md-6">
                                    <input id="parcel" type="text"
                                           class="form-control @error('parcel') is-invalid @enderror" name="parcel"
                                           required>
                                    @error('parcel')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">

                                <label for="parcel"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Series Prefix') }}</label>

                                <div class="col-md-6">
                                    <input id="series_prefix" type="text"
                                           placeholder="2 characters"
                                           class="form-control @error('series_prefix') is-invalid @enderror" name="series_prefix"
                                           required>
                                    @error('series_prefix')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row d-none message-area" id="error_message">
                                <label
                                    class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6 text-danger" id="error_response_message"></div>
                            </div>
                            <div class="form-group row d-none message-area" id="success_message">
                                <label
                                    class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6 text-primary" id="success_response_message"></div>
                            </div>
                            <div class="form-group row mb-0" id="submit-btn-area">
                                <div class="col-md-8 offset-md-4">
                                    <a class="btn btn-primary" href="javascript:void(0)" onclick="return genCode()">
                                        {{ __('Generate Code') }}
                                    </a>

                                </div>
                            </div>
                        </form>

                    </div>
                    <div class="card-body">
                        <i>Please do not close browser while processing</i>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-footer')
    <script type="text/javascript">
        window.onload = function () {
            //$('#success_message').removeClass('d-none');
        };

        function genCode() {
            $('#submit-btn-area').addClass('d-none');
            $('.message-area').addClass('d-none');
            axios.post('/a/start', {
                desire: $('#desire').val(),
                parcel: $('#parcel').val(),
                series_prefix: $('#series_prefix').val()
            }).then((response) => {
                console.log('gen code', response);
                if (typeof response.data.error != 'undefined') {
                    $('#submit-btn-area').removeClass('d-none');
                    $('#error_message').removeClass('d-none');
                    $('#error_response_message').html(response.data.error.message);
                } else {
                    checkGen();
                }
            }, (error) => {
                $('#submit-btn-area').removeClass('d-none');
            });
            return false;
        }

        function checkGen() {
            axios.get('/a/check-gen', {}).then((response) => {
                console.log(response);
                if (response.data.done) {
                    $('#submit-btn-area').removeClass('d-none');
                    //$('.message-area').addClass('d-none');
                    $('#success_response_message').html(response.data.new_generated + '/' + $('#desire').val() + ' DONE');
                    $('#total_span').html(response.data.total_codes);
                } else {
                    $('#success_message').removeClass('d-none');
                    $('#success_response_message').html(response.data.new_generated + '/' + $('#desire').val());
                    $('#total_span').html(response.data.total_codes);
                    setTimeout(function () {
                        checkGen();
                    }, 2000);
                }
            }, (error) => {
                console.log(error);
                $('#submit-btn-area').removeClass('d-none');
                $('.message-area').addClass('d-none');
            });
        }

    </script>
@endsection
