@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Reset Codes') }}</div>

                    <div class="card-body">
                        @if ($deleted)
                            <div class="alert alert-success" role="alert">
                                All data deleted
                            </div>
                        @endif
                        <form method="POST">
                            @csrf
                            <div class="form-group row">
                                <label
                                    class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6">
                                    <span id="total_span">Enter "<span class="text-success">Yes, I do</span>" to text box to reset data</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right"></label>

                                <div class="col-md-6">
                                    <input id="secure_code" type="text"
                                           class="form-control @error('secure_code') is-invalid @enderror" name="secure_code"
                                           value="{{ old('secure_code') }}" required autofocus>

                                    @error('secure_code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row message-area" id="error_message">
                                <label
                                    class="col-md-4 col-form-label text-md-right"></label>
                                <div class="col-md-6 text-danger" id="error_response_message">When reset, all codes will be erased, CANNOT revert</div>
                            </div>

                            <div class="form-group row mb-0" id="submit-btn-area">
                                <div class="col-md-8 offset-md-4">
                                    <button class="btn btn-danger" type="submit">
                                        {{ __('Reset Codes') }}
                                    </button>

                                    <a class="btn btn-link" href="/">
                                        {{ __('Cancel') }}
                                    </a>

                                </div>
                            </div>
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('js-footer')
    <script type="text/javascript">
        window.onload = function () {
            //$('#success_message').removeClass('d-none');
        };

        function genCode() {
            $('#submit-btn-area').addClass('d-none');
            $('.message-area').addClass('d-none');
            axios.post('/start', {
                desire: $('#desire').val(),
                parcel: $('#parcel').val()
            }).then((response) => {
                console.log('gen code', response);
                if (typeof response.data.error != 'undefined') {
                    $('#submit-btn-area').removeClass('d-none');
                    $('#error_message').removeClass('d-none');
                    $('#error_response_message').html(response.data.error.message);
                } else {
                    checkGen();
                }
            }, (error) => {
                $('#submit-btn-area').removeClass('d-none');
            });
            return false;
        }

        function checkGen() {
            axios.get('/check-gen', {}).then((response) => {
                console.log(response);
                if (response.data.done) {
                    $('#submit-btn-area').removeClass('d-none');
                    //$('.message-area').addClass('d-none');
                    $('#success_response_message').html(response.data.new_generated + '/' + $('#desire').val() + ' DONE');
                    $('#total_span').html(response.data.total_codes);
                } else {
                    $('#success_message').removeClass('d-none');
                    $('#success_response_message').html(response.data.new_generated + '/' + $('#desire').val());
                    $('#total_span').html(response.data.total_codes);
                    setTimeout(function () {
                        checkGen();
                    }, 2000);
                }
            }, (error) => {
                console.log(error);
                $('#submit-btn-area').removeClass('d-none');
                $('.message-area').addClass('d-none');
            });
        }

    </script>
@endsection
