@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">{{ __('Config') }}</div>

                    <div class="card-body">
                        @if (0)
                            <div class="alert alert-success" role="alert">
                                Saved
                            </div>
                        @endif
                        <form method="POST">
                            @csrf
                            @if(!$total_codes)
                            <div class="form-group row">
                                <label for="desire"
                                       class="col-md-4 col-form-label text-md-right">Secret Hash</label>

                                <div class="col-md-6">

                                    <input id="parcel" type="text"
                                           class="form-control @error('secret_code_extend') is-invalid @enderror" name="secret_code_extend"
                                           value="{{ $secret_code_extend }}" required autofocus>

                                    @error('secret_code_extend')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0" id="submit-btn-area">
                                <div class="col-md-8 offset-md-4">
                                    <button class="btn btn-danger" type="submit">
                                        {{ __('Save') }}
                                    </button>

                                    <a class="btn btn-link" href="/home">
                                        {{ __('Cancel') }}
                                    </a>

                                </div>
                            </div>
                                @else
                                <div class="form-group row">
                                    <label for="desire"
                                           class="col-form-label text-md-right">Cannot update Secret Code because data is not empty</label>
                                </div>
                            @endif
                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
