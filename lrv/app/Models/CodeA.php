<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CodeA extends Model
{
    protected $connection = 'mysql';
    protected $table = 'hash_codes';
    protected $fillable = ['so_lo', 'series', 'code', 'hash_code'];
}
