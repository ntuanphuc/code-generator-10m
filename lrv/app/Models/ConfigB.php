<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigB extends Model
{
    protected $connection = 'mysql1';
    protected $table = 'hash_config';
    protected $fillable = ['key_name', 'key_value'];
}
