<?php
namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class CodeB extends Model
{
    protected $connection = 'mysql1';
    protected $table = 'hash_codes';
    protected $fillable = ['so_lo', 'series', 'code', 'hash_code'];
}
