<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Config extends Model
{
    protected $table = 'hash_config';
    protected $fillable = ['key_name', 'key_value'];
}
