<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ConfigA extends Model
{
    protected $connection = 'mysql';
    protected $table = 'hash_config';
    protected $fillable = ['key_name', 'key_value'];
}
