<?php

namespace App\Http\Controllers\ProjectB;

use App\Helpers\CodeTool;
use App\Helpers\HashCode;
use App\Jobs\RehashQueue;
use Illuminate\Http\Request;
use App\Jobs\RedisDockQueue;
use App\Models\CodeB as Code;
use App\Models\ConfigB as Config;
use Illuminate\Support\Facades\DB;
use App\Exports\CodeExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Http\Controllers\Controller;
use Goutte\Client as gClient;

class HomeController extends Controller
{
    protected $codeLen = 7;
    protected $projectId = 'b';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $config = Config::where('key_name', 'secret_code_extend')->first();
        if ($config) {
            $secretCodeExtend = $config->key_value;
        } else {
            return redirect(route($this->projectId . '.config'));
        }
        return view('project_' . $this->projectId . '.home', ['total_codes' => Code::count(), 'secret_code_extend' => $secretCodeExtend]);
    }

    public function start(Request $request)
    {
        $config = Config::where('key_name', 'secret_code_extend')->first();

        if ($request->isMethod('POST')) {
            //
            $desire = $request->get('desire');
            $soLo = trim($request->get('parcel'));
            $seriesPrefix = trim($request->get('series_prefix'));

            if (!is_numeric($desire)) {
                return response()->json(['error' => ['message' => 'Number of code must be a numeric']]);
            }
            if ($soLo == '') {
                return response()->json(['error' => ['message' => 'Parcel cannot be empty']]);
            }
            if ($seriesPrefix == '') {
                return response()->json(['error' => ['message' => 'Prefix cannot be empty']]);
            }
            /*if (strlen($seriesPrefix) != 2) {
                return response()->json(['error' => ['message' => 'Prefix must be 2 characters']]);
            }*/
            if ($desire > 1000000) {
                return response()->json(['error' => ['message' => 'Sorry, we only support 1M each']]);
            }

            $startNumber = Code::count();

            session(['start_number_' . $this->codeLen => $startNumber, 'desire_' . $this->codeLen => $desire]);

            $extendData = ['so_lo' => strtoupper($soLo), 'series_prefix' => strtoupper($seriesPrefix)];

            Cache::pull('secret_code_extend');
            $eachTime = 1000;
            $times = ceil($desire / $eachTime);
            for ($i = 0; $i < $times; $i++) {
                $min = min($eachTime, $desire);
                $desire -= $eachTime;
                RedisDockQueue::dispatch($min, $this->codeLen, $extendData);
            }
            return response()->json(['data' => ['started' => 1]]);
        }

        $secretCodeExtend = isset($config->key_value) ? $config->key_value : 0;

        if (!$secretCodeExtend) {

            return redirect(route($this->projectId . '.config'));
        }
        //$config = Config::where('key_name', 'secret_code_extend')->first();
        Cache::put('secret_code_extend', 'L4RUEONPACK2021!');
        if (isset($_GET['db']))
            dd(Cache::get('secret_code_extend'));

        return view('project_' . $this->projectId . '.code_gen.start', ['total_codes' => Code::count(), 'secret_code_extend' => $secretCodeExtend]);
    }

    public function checkGen()
    {

        $startNumber = session('start_number_' . $this->codeLen);
        $desire = session('desire_' . $this->codeLen);
        $dbNumber = Code::count();

        $newGenerated = $dbNumber - $startNumber;
        $done = $newGenerated >= $desire;

        return response()->json([
            'done' => $done,
            'new_generated' => number_format($newGenerated),
            'total_codes' => number_format($dbNumber),
            'desire' => $desire
        ]);
    }


    public function resetData(Request $request)
    {
        $deleted = false;
        if ($request->isMethod('POST')) {
            $secureCode = $request->get('secure_code');
            if ($secureCode == 'Yes, I do') {
                DB::connection('mysql1')->table('hash_codes')->truncate();
                $deleted = true;
            } else {
                return redirect(route($this->projectId . '.reset_data'))->withErrors(['secure_code' => 'Data not reset']);
            }
        }

        return view('project_' . $this->projectId . '.code_gen.reset_data', ['deleted' => $deleted]);
    }

    public function rehashData(Request $request, $parcel)
    {
        $deleted = false;

        $parcel = strtoupper(trim($parcel));

        $config = Config::where('key_name', 'secret_code_extend')->first();
        //get the null hash code (to see if the script finished or not)
        $nullHashCode = DB::connection('mysql1')
            ->table('hash_codes')
            ->where('so_lo', $parcel)
            ->whereNull('hash_code')
            ->count();

        /*$tmp = Code::orderBy('id','asc')->limit(10)->get();
        foreach($tmp as $t){
            echo $t->hash_code . '<br />';
        }
        exit;
        */

        if ($nullHashCode) {
            return view('project_' . $this->projectId . '.code_gen.rehash_data', ['processing' => $nullHashCode, 'parcel' => $parcel]);
        }

        if ($request->isMethod('POST')) {
            $secureCode = $request->get('secure_code');
            if ($secureCode == 'Yes, do rehash') {
                //DB::connection('mysql1')->table('hash_codes')->truncate();
                //$deleted = true;

                $validator = Validator::make($request->all(), [
                    'secret_code_extend' => 'required|min:5'
                ]);

                if ($validator->fails()) {
                    return redirect(route($this->projectId . '.rehash_data', ['parcel' => $parcel]))
                        ->withErrors($validator)
                        ->withInput();
                }

                $config->key_value = trim($request->get('secret_code_extend'));
                $config->save();

                //set null all codes then rehash by new secret code
                DB::connection('mysql1')->table('hash_codes')
                    ->where('so_lo', $parcel)
                    ->update(['hash_code' => null]);

                DB::table('rehash_histories')->insert([
                        'parcel' => $parcel,
                        'new_secret_code' => $config->key_value
                    ]
                );

                RehashQueue::dispatch(
                    1000,
                    $this->codeLen,
                    ['so_lo' => $parcel]
                );

                return redirect(route($this->projectId . '.rehash_data', ['parcel' => $parcel]));

            } else {
                return redirect(route($this->projectId . '.rehash_data', ['parcel' => $parcel]))->withErrors(['secure_code' => 'Cannot rehash data', 'processing' => 0]);
            }
        }

        return view('project_' . $this->projectId . '.code_gen.rehash_data', ['deleted' => $deleted, 'config' => $config, 'processing' => 0]);
    }

    function getUsersOneByOne($parcel, $exportType)
    {
        // build your chunks as you want (200 chunks of 10 in this example)
        $max = Code::where('so_lo', $parcel)->count();
        if (!$max) return null;
        if ($max > 1000)
            $each = 1000;
        else $each = $max;
        $loopNo = $max / $each;
        $exportField = 'id, so_lo, series_prefix, series, ' . (($exportType == 'origin') ? 'code' : 'hash_code');

        for ($i = 0; $i < $loopNo; $i++) {
            $users = DB::connection('mysql1')->table('hash_codes')->select(DB::raw($exportField))->where('so_lo', $parcel)->skip($i * $each)->take($each)->get();
            // Yield user one by one
            foreach ($users as $user) {
                $len = strlen($user->id);
                $addition = '';
                for ($i1 = 0; $i1 < 8 - $len; $i1++) {
                    $addition .= '0';
                }
                $series = $user->series_prefix . $addition . $user->id;
                $user->series = $series;
                unset($user->id, $user->series_prefix);
                yield $user;
            }
        }
    }

    public function export(Request $request)
    {
        if ($request->isMethod('POST')) {

            $parcel = $request->get('parcel');
            $exportType = $request->get('export_type');
            $fileName = $this->codeLen . '.' . $parcel . '-' . $exportType . '.csv';
            (new FastExcel($this->getUsersOneByOne($parcel, $exportType)))->export($fileName);

            return response()->json(['data' => ['file' => $fileName]]);
        }


        $parcels = Code::select(DB::raw('hash_codes.so_lo, COUNT(so_lo) AS every_total'))->groupBy('so_lo')->get();

        return view('project_' . $this->projectId . '.code_gen.exports', ['parcels' => $parcels]);
    }

    public function config(Request $request)
    {
        $config = Config::where('key_name', 'secret_code_extend')->first();
        if ($request->isMethod('POST')) {
            $validator = Validator::make($request->all(), [
                'secret_code_extend' => 'required|min:5'
            ]);

            if ($validator->fails()) {
                return redirect(route($this->projectId . '.config'))
                    ->withErrors($validator)
                    ->withInput();
            }
            //$config = new Config;
            //$config->key_name = 'secret_code_extend';
            $config->key_value = trim($request->get('secret_code_extend'));
            $config->save();
        }


        if (!$config) {
            $config = new Config;
            $config->key_name = 'secret_code_extend';
            $config->key_value = '';
            $config->save();
        }
        $totalCodes = Code::count();

        return view('project_' . $this->projectId . '.code_gen.config', ['secret_code_extend' => $config->key_value, 'total_codes' => $totalCodes]);
    }

    public function delete(Request $request)
    {
        try {
            $parcel = trim($request->get('parcel'));

            DB::connection('mysql1')->table('hash_codes')->where('so_lo', $parcel)->delete();
            if (is_file(base_path('public/7.' . $parcel . '-origin.csv'))) {
                @unlink(base_path('public/7.' . $parcel . '-origin.csv'));
            }

            if (is_file(base_path('public/7.' . $parcel . '-hash-only.csv'))) {
                @unlink(base_path('public/7.' . $parcel . '-hash-only.csv'));
            }
            return response()->json(['data' => ['deleted' => 1]]);
        } catch (\Exception $e) {
            return response()->json(['error' => ['message' => 'Cannot delete']]);
        }
    }

    public function checkValidCode(Request $request)
    {
        $config = Config::where('key_name', 'secret_code_extend')->first();
        if ($request->isMethod('POST')) {
            $validator = Validator::make($request->all(), [
                'secret_code_extend' => 'required|min:5',
                'code' => 'required|min:7',
                'hash' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect(route($this->projectId . '.check_valid_code'))
                    ->withErrors($validator)
                    ->withInput();
            }

            $secret = trim($request->get('secret_code_extend'));
            $code = strtoupper(trim($request->get('code')));
            $hash = $request->get('hash');

            $tool = new HashCode($this->codeLen);
            $isValid = $tool->validCode($code);
            $newHash = $tool->hash($code);
            $isCorrect = false;
            if ($hash == $newHash)
                $isCorrect = true;
            $message = 'Code rule is ' . ($isValid ? 'valid' : 'NOT valid') . ' and hashing is ' . ($isCorrect ? 'correct' : 'Not correct');
            return view('project_' . $this->projectId . '.code_gen.check_valid_code', ['secret_code_extend' => $config->key_value, 'code' => $code, 'hash' => $hash, 'msg' => $message]);
        }
        if (!$config) {
            $config = new Config;
            $config->key_name = 'secret_code_extend';
            $config->key_value = '';
            $config->save();
        }
        return view('project_' . $this->projectId . '.code_gen.check_valid_code', ['secret_code_extend' => $config->key_value, 'msg' => false, 'code' => '', 'hash' => '']);
    }

}
