<?php

namespace App\Http\Controllers\ProjectA;

use Illuminate\Http\Request;
use App\Jobs\RedisDockQueue;
use App\Models\CodeA as Code;
use App\Models\ConfigA as Config;
use Illuminate\Support\Facades\DB;
use App\Exports\CodeExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use Rap2hpoutre\FastExcel\FastExcel;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $config = Config::where('key_name', 'secret_code_extend')->first();
        if ($config) {
            $secretCodeExtend = $config->key_value;
        } else {
            return redirect(route('a.config'));
        }
        return view('project_a.home', ['total_codes' => Code::count(), 'secret_code_extend' => $secretCodeExtend]);
    }

    public function start(Request $request)
    {
        $config = Config::where('key_name', 'secret_code_extend')->first();

        if ($request->isMethod('POST')) {
            //
            $desire = $request->get('desire');
            $soLo = trim($request->get('parcel'));
            $seriesPrefix = trim($request->get('series_prefix'));

            if (!is_numeric($desire)) {
                return response()->json(['error' => ['message' => 'Number of code must be a numeric']]);
            }
            if ($soLo == '') {
                return response()->json(['error' => ['message' => 'Parcel cannot be empty']]);
            }
            if ($seriesPrefix == '') {
                return response()->json(['error' => ['message' => 'Prefix cannot be empty']]);
            }
            if (strlen($seriesPrefix) != 2) {
                return response()->json(['error' => ['message' => 'Prefix must be 2 characters']]);
            }
            if ($desire > 1000000) {
                return response()->json(['error' => ['message' => 'Sorry, we only support 1M each']]);
            }

            $startNumber = Code::count();

            session(['start_number_8' => $startNumber, 'desire_8' => $desire]);

            $extendData = ['so_lo' => $soLo, 'series_prefix' => $seriesPrefix];

            Cache::pull('secret_code_extend');
            $eachTime = 1000;
            $times = ceil($desire / $eachTime);
            for($i = 0; $i < $times; $i++){
                $min = min($eachTime, $desire);
                $desire -= $eachTime;
                RedisDockQueue::dispatch($min, config('code.code_len'), $extendData);
            }
            //RedisDockQueue::dispatch($desire, 6, $extendData);
            return response()->json(['data' => ['started' => 1]]);
        }

        $secretCodeExtend = isset($config->key_value) ? $config->key_value : 0;

        if (!$secretCodeExtend) {

            return redirect(route('config'));
        }

        return view('project_a.code_gen.start', ['total_codes' => Code::count(), 'secret_code_extend' => $secretCodeExtend]);
    }

    public function checkGen()
    {

        $startNumber = session('start_number_8');
        $desire = session('desire_8');
        $dbNumber = Code::count();

        $newGenerated = $dbNumber - $startNumber;
        $done = $newGenerated >= $desire;

        return response()->json([
            'done' => $done,
            'new_generated' => $newGenerated,
            'total_codes' => number_format($dbNumber),
            'desire' => $desire
        ]);
    }


    public function resetData(Request $request)
    {
        $deleted = false;
        if ($request->isMethod('POST')) {
            $secureCode = $request->get('secure_code');
            if ($secureCode == 'Yes, I do') {
                DB::table('hash_codes')->truncate();
                $deleted = true;
            } else {
                return redirect(route('reset_data'))->withErrors(['secure_code' => 'Data not reset']);
            }
        }

        return view('project_a.code_gen.reset_data', ['deleted' => $deleted]);
    }

    function getUsersOneByOne($parcel, $exportType)
    {
        // build your chunks as you want (200 chunks of 10 in this example)
        //$testData = Code::where('so_lo', 'AUG16')->get();
        $max = Code::where('so_lo', $parcel)->count();
        if(!$max) return null;
        if ($max > 1000)
            $each = 1000;
        else $each = $max;
        $loopNo = $max / $each;
        $exportField = 'id, so_lo, series_prefix, series, ' . (($exportType == 'origin') ? 'code' : 'hash_code');

        for ($i = 0; $i < $loopNo; $i++) {
            $users = DB::table('hash_codes')->select(DB::raw($exportField))->where('so_lo', $parcel)->skip($i * $each)->take($each)->get();
            // Yield user one by one
            //$codeLen = config('code.code_len');
            foreach ($users as $user) {
                $len = strlen($user->id);
                $addition = '';
                for ($i1 = 0; $i1 < 8 /*10M with 8 chars*/ - $len; $i1++) {
                    $addition .= '0';
                }
                $series = $user->series_prefix . $addition . $user->id;
                $user->series = $series;
                unset($user->id, $user->series_prefix);
                yield $user;
            }
        }
    }

    public function export(Request $request)
    {
        //dd(Cache::get('secret_code_extend'));
        if ($request->isMethod('POST')) {

            $parcel = $request->get('parcel');
            $exportType = $request->get('export_type');
            $fileName = '9.' . $parcel . '-' . $exportType . '.csv';
            (new FastExcel($this->getUsersOneByOne($parcel, $exportType)))->export($fileName);

            return response()->json(['data' => ['file' => $fileName]]);
            /*$validator = Validator::make($request->all(), [
                'no_of_codes' => 'required|integer|max:500000',
                'parcel' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect(route('export_data'))
                    ->withErrors($validator)
                    ->withInput();
            }*/
            //return Excel::download(new CodeExport($request->get('no_of_codes'), trim($request->get('parcel')), $request->get('export_type')), 'codes-' . date('d-M_H-i') . '.xlsx');
        }


        $parcels = Code::select(DB::raw('hash_codes.so_lo, COUNT(so_lo) AS every_total'))->groupBy('so_lo')->get();

        return view('project_a.code_gen.exports', ['parcels' => $parcels]);
    }

    public function config(Request $request)
    {
        $config = Config::where('key_name', 'secret_code_extend')->first();
        if ($request->isMethod('POST')) {
            $validator = Validator::make($request->all(), [
                'secret_code_extend' => 'required|min:5'
            ]);

            if ($validator->fails()) {
                return redirect(route('config'))
                    ->withErrors($validator)
                    ->withInput();
            }
            //$config = new Config;
            //$config->key_name = 'secret_code_extend';
            $config->key_value = trim($request->get('secret_code_extend'));
            $config->save();
        }


        if (!$config) {
            $config = new Config;
            $config->key_name = 'secret_code_extend';
            $config->key_value = '';
            $config->save();
        }
        $totalCodes = Code::count();

        return view('project_a.code_gen.config', ['secret_code_extend' => $config->key_value, 'total_codes' => $totalCodes]);
    }

}
