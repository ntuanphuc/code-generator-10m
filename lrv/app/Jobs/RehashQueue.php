<?php

namespace App\Jobs;

use App\Helpers\CodeTool;
use App\Helpers\HashCode;
use App\Models\Code;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class RehashQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $desireCodes = 1000;
    protected $recordsPerRunning = 1000;
    protected $codeLen = 0;
    protected $extendData = [];

    /**
     * Create a new job instance.
     *
     * @param integer $desireCodes
     * @param integer $codeLen
     * @param array $extendData
     */
    public function __construct($desireCodes = 0, $codeLen = 0, $extendData = [])
    {
        $this->desireCodes = $desireCodes;
        $this->codeLen = $codeLen;
        $this->extendData = $extendData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Log::debug('An informational message');
        //CodeTool::genCode(min($this->recordsPerRunning, $this->desire), $this->codeLen, $this->extendData);
        $limit = $this->desireCodes;
        $soLo = $this->extendData['so_lo'] ?? false;

        $codes = Code::whereNull('hash_code');

        if ($soLo)
            $codes = $codes->where('so_lo', $soLo);

        $codes = $codes
            ->orderBy('id', 'asc')
            ->limit($limit)
            ->get();

        if ($codes) {
            $hashTool = new HashCode($this->codeLen);
            foreach ($codes as $code) {
                $hashCode = $hashTool->hash($code->code);
                $code->hash_code = $hashCode;
                $code->save();
            }
            //find if there are some null codes left
            $nullCodes = Code::whereNull('hash_code');
            if ($soLo)
                $nullCodes = $nullCodes->where('so_lo', $soLo);
            $count = $nullCodes->count();

            if ($count) {
                RehashQueue::dispatch(
                    $this->desireCodes,
                    $this->codeLen,
                    $this->extendData
                )->delay(3);
            }
        }

    }
}
