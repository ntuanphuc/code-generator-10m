<?php

namespace App\Jobs;

use App\Helpers\CodeTool;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class RedisDockQueue implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $desire = 1000;
    protected $recordsPerRunning = 1000;
    protected $codeLen = 0;
    protected $extendData = [];

    /**
     * Create a new job instance.
     *
     * @param integer $desire
     * @param integer $codeLen
     * @param array $extendData
     */
    public function __construct($desire = 0, $codeLen = 0, $extendData = [])
    {
        $this->desire = $desire;
        $this->codeLen = $codeLen;
        $this->extendData = $extendData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //Log::debug('An informational message');
        CodeTool::genCode(min($this->recordsPerRunning, $this->desire), $this->codeLen, $this->extendData);

        $leftRecords = $this->desire - $this->recordsPerRunning;

        if ($leftRecords > 0) {
            //RedisDockQueue::dispatch($leftRecords, $this->codeLen, $this->extendData); //call itself
        }
    }
}
