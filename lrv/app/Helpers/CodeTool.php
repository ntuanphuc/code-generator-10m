<?php

namespace App\Helpers;

use App\Models\Code;
use Illuminate\Support\Facades\DB;

class CodeTool
{
    public static function genCode($quantity, $codeLen, $extendData)
    {

        $saved = 0;
        $duplicated = 0;
        $hashTool = new HashCode($codeLen);
        $dbServer = 'mysql';
        if($codeLen == 7) $dbServer = 'mysql1';
        while ($saved < $quantity) {
            $code = $hashTool->generateCode();
            $hashCode = $hashTool->hash($code);

            /*$codeCls = new Code;
            $codeCls->code = $code;
            $codeCls->hash_code = $hashCode;*/
            $data = [
              'code' => $code,
              'hash_code' => $hashCode
            ];
            if (!empty($extendData)) {
                foreach($extendData as $k => $v){
                    //$codeCls->$k = $v;
                    $data[$k] = $v;
                }
            }

            try {
                DB::connection($dbServer)->table('hash_codes')->insert(
                    $data
                );
                //$codeCls->save();
                $saved++;
            } catch (\Exception $e) {
                //echo $e->getMessage();
                $duplicated++;
            }
        }

    }
}
