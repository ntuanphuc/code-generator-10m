<?php

namespace App\Helpers;

use App\Models\Config;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class HashCode
{
    private $allowedCharsArr = [
        'A' => 1, 'C' => 2, 'E' => 3, 'F' => 4, 'G' => 5, 'H' => 6,
        'J' => 7, 'K' => 8, 'L' => 9, 'M' => 10, 'N' => 11, 'P' => 12,
        'R' => 13, 'S' => 14, 'T' => 15, 'U' => 16, 'V' => 17, 'W' => 18,
        'X' => 19, 'Y' => 20, '3' => 21, '4' => 22, '5' => 23, '6' => 24,
        '7' => 25, '9' => 26];
    private $codeLen = 8;
    private $secureNumber = 10;
    private $allowedCharsStr = 'ACEFGHJKLMNPRSTUVWXY345679';

    public function __construct($codeLen = 0)
    {
        if(!$codeLen){
            //$config = Config::where('key_name', 'code_len')->first();
            //if($config)
            //    $codeLen = $config->key_value - 1;
            $codeLen = env('CODE_LEN', 8);
        }
        $this->codeLen = $codeLen;
    }

    public function getAllowedCharsAsArr()
    {
        return $this->allowedCharsArr;
    }

    public function getCodeLen()
    {
        return $this->codeLen;
    }

    public function setAllowedCharsAsArr($chars/* array */)
    {
        $this->allowedCharsArr = $chars;
    }

    public function setCodeLen($len)
    {
        $this->codeLen = $len;
    }

    public function initConfig()
    {

    }

    /**
     * check input code is valid or not
     * @param $code
     * @return boolean
     */
    public function validCode($code)
    {
        $code = strtoupper(trim($code));
        $len = strlen($code);
        if ($len != ($this->codeLen)) {
            return false;
        }
        //check allowed characters
        if (preg_match('/[^' . $this->allowedCharsStr . ']/i', $code)) {
            return false;
        }
        //checksum
        return $this->calculateSum($code);
    }

    /**
     * calculate checksum from second character to semi last character of code, then compare with last character
     * @param $code
     * @return bool
     */
    public function calculateSum($code)
    {
        $code = strtoupper(trim($code));
        $checksum = 0;
        //$codeLen = $this->codeLen; //8;//strlen($code); = len - 1
        $codeLen = strlen($code) - 1;
        for ($i = $codeLen; $i >= 1; $i--) {
            $checksum += $this->allowedCharsArr[substr($code, $codeLen - $i, 1)] * $i;
        }
        $checksum += $this->secureNumber;
        //echo $checksum . ' % 25 = ' . ($checksum % 25) . "\n";
        $checksum = ($checksum % 25);

        //echo "CS: " . substr($code, $codeLen, 1) . " vs " . substr($this->allowedCharsStr, $checksum, 1) . "\n";
        return (substr($this->allowedCharsStr, $checksum, 1) == substr($code, $codeLen, 1));
    }

    public function generateCode()
    {
        $allowedCharsArrLen = count($this->allowedCharsArr);
        $checksum = 0;
        $returnCode = '';
        $codeLen = $this->codeLen - 1;

        for ($i = $codeLen; $i >= 1; $i--) {
            $randChar = substr($this->allowedCharsStr, rand(0, $allowedCharsArrLen - 1), 1);
            $returnCode .= $randChar;
        }
        for ($i = $codeLen; $i >= 1; $i--) {
            $checksum += $this->allowedCharsArr[substr($returnCode, $codeLen - $i, 1)] * $i;
        }

        $checksum += $this->secureNumber; //more secure
        $checksum = $checksum % 25;

        return $returnCode . substr($this->allowedCharsStr, $checksum, 1);
    }

    public function hash($code)
    {
        $dbServer = ($this->codeLen == 7) ? 'mysql1' : 'mysql';
        $secretCodeExtend = Cache::get('secret_code_extend_' . $this->codeLen);
        if (!$secretCodeExtend) {
            $config = DB::connection($dbServer)->table('hash_config')->where('key_name', 'secret_code_extend')->first();
            if ($config) {
                Cache::put('secret_code_extend_' . $this->codeLen, $config->key_value);
                $secretCodeExtend = $config->key_value;
            } else {
                return false;
            }
        }

        return hash('sha256', trim($secretCodeExtend . trim(strtoupper($code))));
    }

}
