<?php
namespace App\Exports;

use App\Models\Code;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class CodeExport implements FromView
{


    protected $quantity = 1000;
    protected $parcel = null;
    protected $exportType = null;

    public function __construct($quantity, $parcel, $exportType)
    {
        $this->quantity = $quantity;
        $this->parcel = $parcel;
        $this->exportType = $exportType;
    }

    public function view(): View
    {
        return view('exports.codes', [
           'codes' => Code::where('so_lo', $this->parcel)->limit($this->quantity)->get(),
            'export_type' => $this->exportType
        ]);
    }
}
