<?php

namespace App\Console\Commands;

use App\Helpers\HashCode;
use App\Jobs\RedisDockQueue;
use App\Models\Code;
use Illuminate\Console\Command;

class TestRedisCmd extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cmd:test_redis';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        //for ($i = 0; $i < 5; $i++) {

        //}

        /*echo "Inserted: " . $saved . "\n";
        echo "Duplicated: " . $duplicated . "\n";
        echo number_format(microtime(true) - $start) . " seconds\n";*/
        //RedisDockQueue::dispatch(10);
        $hashTool = new HashCode(6);
        $code = $hashTool->generateCode();
        $isValid = $hashTool->validCode($code);
        echo $code . "\n";
        echo "Len: " . strlen($code) . "\n";
        if($isValid)
            $this->info('IS VALID');
        else  $this->warn('NOT VALID');
        echo "\n";
        return 0;
    }
}
