<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHashCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hash_codes', function (Blueprint $table) {
            $table->id(); //this will be used as series
            $table->char('so_lo', 40)->nullable();
            $table->integer('series')->default(0);
            $table->char('code', 20)->nullable();
            $table->char('hash_code', 65)->unique()->index('unique_hash_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hash_codes');
    }
}
