<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome'); //select project
});
Route::get('/home', function () {
    return view('welcome'); //select project
});


/*Route::get('/a', function () {
    return view('welcome');
});
Route::get('/b', function () {
    return view('welcome');
});*/

Auth::routes();

Route::namespace('ProjectA')->group(function () {
    Route::prefix('a')->group(function () {
        Route::get('/home', 'HomeController@index')->name('a.home');

        Route::match(['GET', 'POST'], '/start', 'HomeController@start')->name('a.start');

        Route::get('/check-gen', 'HomeController@checkGen')->name('a.check_gen');

        Route::match(['GET', 'POST'], '/reset-data', 'HomeController@resetData')->name('a.reset_data');

        Route::match(['GET', 'POST'], '/exports', 'HomeController@export')->name('a.export_data');

        Route::match(['GET', 'POST'], '/config', 'HomeController@config')->name('a.config');
    });
});
Route::namespace('ProjectB')->group(function () {
    Route::prefix('b')->group(function () {
        Route::get('/home', 'HomeController@index')->name('b.home');

        Route::match(['GET', 'POST'], '/start', 'HomeController@start')->name('b.start');

        Route::get('/check-gen', 'HomeController@checkGen')->name('b.check_gen');

        Route::match(['GET', 'POST'], '/reset-data', 'HomeController@resetData')->name('b.reset_data');
        Route::match(['GET', 'POST'], '/rehash-data/{parcel}', 'HomeController@rehashData')->name('b.rehash_data');

        Route::match(['GET', 'POST'], '/exports', 'HomeController@export')->name('b.export_data');
        Route::match(['GET', 'POST'], '/delete', 'HomeController@delete')->name('b.delete_parcel');

        Route::match(['GET', 'POST'], '/check-valid-code', 'HomeController@checkValidCode')->name('b.check_valid_code');

        Route::match(['GET', 'POST'], '/config', 'HomeController@config')->name('b.config');


    });
});
